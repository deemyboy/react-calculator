# React Calculator

First go at building an SPA application in React

### calc.deemyweb.co.uk

### Prerequisites

Must have Node installed

[Get Node](https://nodejs.org/en/)

### Setup

1. Navigate to/or create a React projects folder
2. Clone this project as you normally would using git cli (get the URL from the "clone" link above)
3. Switch to the "react calculator" folder that gets created
4. Run _"npm i"_ or _"npm install"_ (they're equivalent!)
5. Once done, in the same folder (the "root" folder of your new react calculator project, type _"npm start"_ and a webpage should open on localhost:3000 showing the running calculator.

### Images

![react calculator image](src/react-calculator.png)

### Useful links

#### Recommended

[Chrome extension: React Developer Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) if you're thinking about learning React or building a few projects.

[VS Code](https://code.visualstudio.com/) - currently my editor of choice

[Prettier](https://github.com/prettier/prettier-vscode) _"Prettier is an opinionated code formatter. It enforces a consistent style by parsing your code and re-printing it with its own rules that take the maximum line length into account, wrapping code when necessary."_ - from Prettier

#### Handy

[React Crash Course 2019](https://www.youtube.com/watch?v=Ke90Tje7VS0) (YouTube - Mosh Hamedani) - 2 hrs of brilliance - built this (my first React SPA) after watch that so ... i watched it a few times until I could build the project without the video which gave me some what of firmer grounding.

He also gives some good tips on handy VS Code extensions and config tips
