alias ht="cd /c/xampp/htdocs"
alias rt="cd /c/xampp/htdocs/rentandride"
alias gs="git status"
alias ga="git add ."
alias gc="git commit"
alias gcm="git commit -m"
alias gm="git merge"
alias gp="git pull"
alias gb="git branch"
alias gps="git push"
alias gco="git checkout"
alias gcb="git checkout -b"
alias com="git checkout master"
alias pa="php artisan"
alias pas="php artisan serve"
alias rd="cd /c/Users/user/learning/react"

test -f ~/.profile && . ~/.profile
test -f ~/.bashrc && . ~/.bashrc
