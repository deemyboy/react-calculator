import React from "react";
import "./key.scss";

const Key = (props) => {
  const { ky } = props;

  return (
    <button
      onClick={() => props.onClick(ky.id)}
      id={ky.id}
      className={ky.classes}
      title={ky.title}
    >
      {ky.uniChar && typeof ky.uniChar === "string" ? ky.uniChar : ky.value}
    </button>
  );
};
export default Key;
