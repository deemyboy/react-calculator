import React, { Component } from "react";
import "./App.scss";
import Key from "./components/key";
import Display from "./components/display";

class App extends Component {
  displayRef = React.createRef();
  state = {
    numClass: "btn-primary",
    funcClass: "btn-secondary",
    keyBaseClasses: "btn btn-lg btn-key ",
    gridBaseClasses: "d-flex justify-content-around flex-wrap grid",
    keys: [
      {
        id: 0,
        value: "0",
        title: "zero",
        keycode: 48,
        type: "num",
      },
      {
        id: 1,
        value: "1",
        title: "one",
        keycode: 49,
        type: "num",
      },
      {
        id: 2,
        value: "2",
        title: "two",
        keycode: 50,
        type: "num",
      },
      {
        id: 3,
        value: "3",
        title: "three",
        keycode: 51,
        type: "num",
      },
      {
        id: 4,
        value: "4",
        title: "four",
        keycode: 52,
        type: "num",
      },
      {
        id: 5,
        value: "5",
        title: "five",
        keycode: 53,
        type: "num",
      },
      {
        id: 6,
        value: "6",
        title: "six",
        keycode: 54,
        type: "num",
      },
      {
        id: 7,
        value: "7",
        title: "seven",
        keycode: 55,
        type: "num",
      },
      {
        id: 8,
        value: "8",
        title: "eight",
        keycode: 56,
        type: "num",
      },
      {
        id: 9,
        value: "9",
        title: "nine",
        keycode: 57,
        type: "num",
      },
      {
        id: 10,
        value: ".",
        title: "dot",
        keycode: 190,
        type: "num",
      },
      {
        id: 19,
        value: "m",
        uniChar: "\u00B1",
        title: "plus minus (m)",
        keycode: 189,
        type: "num",
      },
      {
        id: 11,
        value: "+",
        title: "plus",
        keycode: 187,
        type: "func",
      },
      {
        id: 12,
        value: "-",
        title: "minus",
        keycode: 189,
        type: "func",
      },
      {
        id: 13,
        value: "x",
        uniChar: "\u00D7",
        title: "multiply (x)",
        keycode: 88,
        type: "func",
      },
      {
        id: 14,
        value: "/",
        uniChar: "\u00F7",
        title: "divide (/)",
        keycode: 191,
        type: "func",
      },
      {
        id: 15,
        value: "^",
        uniChar: "\uD835\uDC65\u00B2",
        title: "square (^)",
        keycode: 54,
        type: "func",
      },
      {
        id: 21,
        value: "r",
        uniChar: "\u00B2\u221A",
        title: "square root (r)",
        keycode: 82,
        type: "func",
      },
      {
        id: 20,
        value: "y",
        uniChar: "\uD835\uDC65\u02B8",
        title: "x to the power y (y)",
        keycode: 89,
        type: "func",
      },
      {
        id: 16,
        value: "=",
        specialClass: "btn-success",
        title: "equals",
        keycode: 187,
        type: "func",
      },
      {
        id: 17,
        value: "c",
        specialClass: "btn-danger",
        title: "clear last keypress (c)",
        keycode: 67,
        type: "func",
      },
      {
        id: 18,
        value: "a",
        uniChar: "\u0061\u0063",
        specialClass: "btn-danger",
        title: "all clear (a)",
        keycode: 65,
        type: "func",
      },
      {
        id: 23,
        value: "l",
        title: "clear console (l)",
        keycode: 76,
        type: "func",
      },
    ],
    keyboardTypes: ["num", "func"],
    buffer1: "",
    buffer2: "",
    condition: 0,
    mathsOps: ["+", "-", "x", "/", "y", "^", "r"],
    doubleBufferMathsOps: ["+", "-", "x", "/", "y"],
    singleBufferMathsOps: ["^", "r"],
    funcOps: ["=", "a", "c", "l"],
    nextOperator: "",
    operator: null,
    prevOperator: "",
    readyForMaths: false,
    displaySectionIndices: ["0", "2"],
    displaySectionLabels: ["calc", "res"],
    disp: {
      class: "display",
    },
    calc: {
      class: "calculation",
      disabledClass: "text-muted",
      normalClass: "calculation",
      sectionWidth: 0,
      widestCharWidth: 0,
      displayRefIndex: 0,
    },
    res: {
      class: "result",
      sectionWidth: 0,
      widestCharWidth: 0,
      displayRefIndex: 2,
    },
    result: null,
    headerTitle: "Reaculator",
    author: "by Abdullah Sarwar",
    headerLink: "https://bitbucket.org/deemyboy/react-calculator/src/master/",
    linkText: "BitBucket",
    linkIcon: "fab fa-bitbucket",
    linkTooltip: "Reculator @ BitBucket",
  };

  setupKeyboards = () => {
    const { keyboardTypes } = this.state;

    const buildKyBrd = (kbtyp) => {
      let grdClasses = this.state.gridBaseClasses;
      grdClasses += " " + kbtyp + "Grid";
      return (
        <React.Fragment key={Math.random()}>
          <div className="col">
            <div className={grdClasses}>{this.setupKeys(kbtyp)}</div>
          </div>
        </React.Fragment>
      );
    };
    let kybrds = [];
    keyboardTypes.forEach(function (kbt) {
      kybrds.push(buildKyBrd(kbt));
    });
    return kybrds;
  };

  setupKeys = (type) => {
    const { keys } = this.state;
    // filter keys by key type num or func
    let filteredKeys = keys.filter((k) => k.type === type);

    const addButtonAttributes = (typez, keysz) => {
      let kysz = [];
      keysz.map((kzy) => {
        let { ...kz } = kzy;
        kz.key = kz.id;
        kz.classes = "";
        kz.classes += this.state.keyBaseClasses;
        kz.classes += this.state[typez + "Class"];
        kz.classes +=
          kz.specialClass !== undefined ? " " + kz.specialClass : "";
        kz.onClick =
          "handle" +
          typez.charAt(0).toUpperCase() +
          typez.substring(1) +
          "KeyEvent";
        kysz.push(kz);
      });
      return kysz;
    };
    // add classes and onclick to buttons
    let kys = addButtonAttributes(type, filteredKeys);
    return kys.map((ky) => {
      return React.createElement(Key, {
        key: ky.id,
        ky: ky,
        onClick: this[ky.onClick],
      });
    });
  };

  UNSAFE_componentWillMount = () => {
    document.addEventListener("keydown", (event) => this.handleKeyPress(event));
  };

  handleKeyPress = (event) => {
    // stop multiple key events on key held down
    if (event.repeat) {
      return;
    }

    this.addActiveClass(event, this.removeActiveClass);
    let key = event.key;

    let pressedKey = this.state.keys.filter(
      (k) => k.value.toString().toLowerCase() === key
    )[0];

    if (pressedKey) {
      if (pressedKey.type === "num") {
        this.handleNumKeyEvent(pressedKey.id);
      } else if (pressedKey.type === "func") {
        this.handleFuncKeyEvent(pressedKey.id);
      }
    }
  };

  addActiveClass = (e, callback) => {
    let target = null;
    let classes = "";

    let { keys } = this.state;
    let _keyObjArray = keys.filter(function (k) {
      if (k.keycode === e.keyCode && e.key === k.value) {
        return k;
      }
    });

    if (_keyObjArray[0] && _keyObjArray[0].hasOwnProperty("id")) {
      target = document.getElementById(_keyObjArray[0].id);
    }

    if (target) {
      classes = target.getAttribute("class");
      if (!classes.split(" ").includes("active")) {
        classes += " active";
      }
      target.setAttribute("class", classes);

      setTimeout(function () {
        callback(target);
      }, 125);
    }
  };

  removeActiveClass = (target) => {
    let classes = target.getAttribute("class");
    classes = classes.split(" ");

    let index = classes.indexOf("active");
    let finalClasses = "";

    if (index > -1) {
      classes.splice(index, 1);
      for (let i = 0; i < classes.length; i++) {
        finalClasses += classes[i] + " ";
      }
    }
    target.setAttribute("class", finalClasses.trim());
  };

  componentWillUnmount = () => {
    document.removeEventListener("keydown", this.handleKeyPress);
  };

  getBufferToUpdate = (condition) => {
    switch (condition) {
      case 0:
        return "buffer1";
      case 1:
        return "buffer1";
      case 2:
        return "buffer2";
      case 3:
        return "buffer2";
      case 4:
        return "buffer1";
      case 5:
        return "buffer1";
      case 6:
        return "buffer2";
      case 7:
        return "buffer2";
      default:
        return "buffer1";
    }
  };

  getDisplayCalculation = () => {
    let operatorChar = "";
    const { buffer1, buffer2, operator } = this.state;
    console.log(
      "getDisplayCalcContent hit",
      "buffer1",
      buffer1,
      "buffer2",
      buffer2,
      "operator",
      operator
    );
    if (operator) {
      operatorChar = this.getUniChar(operator);
    }
    return buffer1 + operatorChar + buffer2;
  };

  getDisplayResult = () => {
    console.log("getDisplayResult hit");
    const { result } = this.state;
    if (!result) {
      return "0";
    } else {
      return result;
    }
  };

  handleDecimalPoint = function (value) {
    console.log("handleDecimalPoint hit", value, value.split("."));

    if (value === "") {
      value = "0.";
    } else if (value.split(".").length === 1) {
      value += ".";
    }
    return value;
  };

  handlePlusMinusKey = (value) => {
    if (value) {
      if (value.charAt(0) === "-") {
        value = value.substring(1);
      } else {
        value = "-" + value;
      }
    }
    return value;
  };

  getKeyValueFromId = (id) => {
    return this.state.keys.filter((key) => {
      return key.id === id;
    })[0].value;
  };

  updateBuffer = (bd) => {
    let buffer = this.state[bd.label];
    console.log(buffer, bd);
    buffer += bd.value;
    this.setState({ [bd.label]: bd.value }, () => {
      this.setCondition();
    });
  };

  updateOperator = (od) => {
    let operator = this.state[od.label];
    console.log(operator);
    operator += od.value;
    this.setState({ [od.label]: od.value }, () => {
      if (od.setCond) {
        this.setCondition();
      }
    });
  };

  handleNumKeyEvent = (id) => {
    console.log("handleNumKeyEvent", id);
    const removeLeadingZero = (data) => {
      if (
        data.value.indexOf(".") === -1 && // not a decimal
        data.value.charAt(0) === "0" && // leading zero present
        data.value.length > 1 // at least 2 chars in length
      ) {
        data.value = data.value.substring(1);
      }
      return data;
    };

    const keyValue = this.getKeyValueFromId(id);
    let bufferData = {};
    bufferData.label = this.getBufferToUpdate(this.state.condition);
    bufferData.value = this.state[bufferData.label];
    // max width reached
    if (this.state.maxWidthReached) {
      if (this.state.calc.state !== "disabled") {
        this.setSectionClass("calc", "disabled");
      }
      return;
    }

    //handle 0 pressed repeatedly
    if (bufferData.value === "0" && keyValue === "0") {
      return;
    }

    // handle numerical input 0-9
    if (!isNaN(+keyValue)) {
      bufferData.value += keyValue;
    } else if (keyValue === "m") {
      //handle +/-
      bufferData.value = this.handlePlusMinusKey(bufferData.value);
    } else if (keyValue === ".") {
      // handle decimal point
      bufferData.value = this.handleDecimalPoint(bufferData.value);
    } else if (keyValue === "." && bufferData.value.indexOf(".") > -1) {
      bufferData.noUpdate = true;
    }

    if (!bufferData.noUpdate) {
      bufferData = removeLeadingZero(bufferData);
      this.updateBuffer(bufferData);
    }
  };

  handleSqrt = () => {
    console.log("handleSqrt hit");
    const { condition } = this.state;
    const _operator = "r";

    switch (condition) {
      case 0:
        return;
      case 1:
      case 5:
        this.setState(
          { operator: _operator, readyForMaths: true },
          this.doTheMath
        );
        return;
      case 2:
        this.setState({ readyForMaths: true }, this.doTheMath);
        return;
      case 3:
        return;
      case 4:
        return;

      default:
        break;
    }

    if (condition === 1 || condition === 2) {
    }
  };

  handleXY = () => {
    const { condition } = this.state;
    const { buffer1 } = this.state;
    const { buffer2 } = this.state;
    const _operator = "y";

    switch (condition) {
      case 0:
        return;
      case 1:
        this.setState(
          {
            operator: _operator,
            prevOperator: _operator,
            buffer1: buffer1,
          },
          () => this.updateDisplayData(_operator)
        );
        break;
      case 2:
        this.setState(
          {
            operator: _operator,
            buffer1: buffer1,
            buffer2: buffer2,
          },
          () => this.updateDisplayData(_operator)
        );
        break;
      //   case 3:
      //     this.setState({ readyForMaths: true }, this.doTheMaths);
      //     break;
      //   case 4:
      //     break;

      default:
        break;
    }
  };

  handleXSquared = () => {
    console.log("handleXSquared hit");
    const { condition } = this.state;
    const _operator = "^";
    switch (condition) {
      case 0:
        return;

      case 1:
      case 5:
        this.setState(
          { operator: _operator, readyForMaths: true },
          this.doTheMath
        );
        break;

      case 2:
        this.setState({ readyForMaths: true }, this.doTheMath);
        break;

      case 3:
        return;

      case 4:
        return;

      default:
        break;
    }
    return;
  };

  processFloat = (label) => {
    const buffer = {};
    buffer.label = label;
    buffer.value = this.state[buffer.label];

    var valueParts = buffer.value.split(".");

    if (
      valueParts.length > 1 &&
      (!valueParts[1] || valueParts[1].length === 0)
    ) {
      buffer.value = valueParts[0];
    }
    return this.removeRedundantZeros(buffer.value);
    // valueParts[0] = this.removeRedundantZeros(valueParts[0]);
    // this.updateDisplayData();
  };

  removeRedundantZeros = (value) => {
    if (Number(value) === parseFloat(value)) {
      return parseFloat(value) + "";
    }
  };

  isBufferFalsey = (label) => {
    console.log("isBufferFalsey hit", label);
    let buffer = this.state[label];
    if (!buffer || buffer === "") {
      return true;
    } else {
      return false;
    }
  };

  handleFuncKeyEvent = (keyId) => {
    let { condition } = this.state;
    const newOperator = this.state.keys.filter((k) => k.id === keyId)[0].value;
    console.log(
      "handleFuncKeyEvent hit: newOperator: ",
      newOperator,
      " : condition: ",
      condition,
      " : keyId: ",
      keyId
    );

    if (condition === 0) {
      return;
    }
    // else if (condition === 1 && this.isDoubleBufferMathsOps(newOperator)) {
    //   return;
    // }

    if (this.state.mathsOps.some((mo) => mo === newOperator)) {
      console.log("returned from isMathsOp: true");
      this.doMathsOp(newOperator);
    } else {
      console.log("returned from isMathsOp: false");
      this.doFuncOp(newOperator);
    }
  };

  doMathsOp = (freshOperator) => {
    const currentOperator = this.state.operator;
    let { condition } = this.state;
    console.log(
      "682: doMathsOp hit - freshOperator: ",
      freshOperator,
      "condition",
      condition
    );
    const label = this.getBufferToUpdate(condition);

    // new calculation
    if (!this.state.operator || this.state.operator === "") {
      this.setState(
        {
          operator: freshOperator,
        },
        () => {
          this.setCondition();
        }
      );
    }

    const { prevOperator } = this.state;
    const buffer = {};

    buffer.label = this.getBufferToUpdate(condition);
    buffer.value = this.state[buffer.label] + "";

    console.log(buffer, freshOperator);

    if (buffer.value && buffer.value.split(".").length > 1) {
      buffer.value = this.processFloat(buffer.label);
    }

    if (
      this.state.result !== "err" &&
      this.isDoubleBufferMathsOps(freshOperator)
    ) {
      switch (this.state.condition) {
        case 0:
          console.log("hit", this.state.condition);
          break;
        case 1:
          this.setState({ operator: freshOperator });
          break;
        case 2:
          console.log(
            "doMathsOp cond: 2 operator: ",
            this.state.operator,
            "freshOperator",
            this.state.freshOperator
          );
          this.setState({
            operator: freshOperator,
            prevOperator: currentOperator,
          });
          break;
        case 3:
          this.setState(
            {
              nextOperator: freshOperator,
              prevOperator: currentOperator,
              readyForMaths: true,
            },
            this.doTheMath
          );
          break;
        case 4:
          return;
        case 5:
          this.setState(
            {
              operator: freshOperator,
            },
            this.updateDisplayData
          );
          return;
        case 6:
          this.setState(
            {
              nextOperator: freshOperator,
              prevOperator: currentOperator,
              readyForMaths: true,
            },
            this.doTheMath
          );
          return;
        case 7:
          this.setState(
            {
              operator: freshOperator,
            },
            this.updateDisplayData
          );
          return;
        default:
          console.log(
            "cannot handle condition",
            this.state.condition,
            "freshOperator",
            freshOperator
          );
          break;
      }
    } else if (freshOperator === "^") {
      this.handleXSquared();
    } else if (freshOperator === "r") {
      this.handleSqrt();
    }
  };

  doFuncOp = (operator) => {
    console.log("doFuncOp hit", operator);

    switch (operator) {
      case "a":
        this.clearAll();
        break;
      case "l":
        console.clear();
        break;
      case "c":
        this.clearLastChar();
        break;
      case "=":
        if (
          (this.state.condition === 3 || this.state.condition === 6) &&
          this.state.result !== "err"
        ) {
          this.setState(
            { readyForMaths: true, nextOperator: operator },
            this.doTheMath
          );
        }
        break;
      default:
        break;
    }
  };

  getCurrentOperator = () => {
    if (this.state.operator) return this.state.operator;
    else if (this.state.prevOperator && this.state.operator)
      return this.state.operator;
    else if (this.state.prevOperator && !this.state.operator)
      return this.state.prevOperator;
    else if (!this.state.prevOperator && !this.state.operator) return;
  };

  clearLastChar = () => {
    return;
    let { buffer1 } = this.state;
    let { buffer2 } = this.state;
    let { result } = this.state;
    let { calculation } = this.state;
    let _operator = this.getCurrentOperator();
    let trimLength = 0;

    const setTrimLength = function (buffer) {
      if (Math.sign(buffer) === -1 && buffer.length === 2) {
        return 2;
      } else {
        return 1;
      }
    };

    // if err we want to clear all
    if (result === "err") {
      this.clearAll();
      return;
    }

    switch (this.state.condition) {
      case 0:
        break;
      case 1:
        console.log(
          "case1: calling setTrimLength with buffer 1",
          buffer1,
          "operator",
          _operator
        );
        trimLength = this.setTrimLength(buffer1, _operator);

        if (!result) {
          this.setState(
            { buffer1: buffer1.substring(0, buffer1.length - trimLength) },
            this.updateDisplayData
          );
        } else if (
          this.state.prevOperator === "^" ||
          this.state.prevOperator === "r" ||
          this.state.prevOperator === "y"
        ) {
          console.log(
            'clearLastChar this.state.prevOperator === "y" cond1',
            this.state.prevOperator === "y",
            "orig trimLength",
            trimLength
          );
          if (this.state.prevOperator === "y") {
            // trimLength += 2; // xy = 2 chars
            this.setState({ operator: this.state.prevOperator });
            console.log(
              'clearLastChar this.state.prevOperator === "y" cond1',
              this.state.prevOperator === "y",
              "mod trimLength",
              trimLength
            );
          }
          this.setState(
            {
              calculation: calculation.substring(
                0,
                calculation.length - trimLength
              ),
              result: 0,
              buffer1: calculation.substring(
                0,
                calculation.length - trimLength
              ),
            },
            this.updateDisplayData
          );
        } else {
          this.setState({ result: 0 }, this.updateDisplayData);
        }
        break;
      case 2:
        console.log(
          "cond: 2 calling setTrimLength with buffer 2",
          buffer2,
          "_operator",
          _operator
        );
        trimLength = this.setTrimLength(buffer2, _operator);
        console.log(
          'clearLastChar this.state.prevOperator === "y" cond1',
          this.state.prevOperator === "y",
          "mod trimLength",
          trimLength
        );
        this.setState({ operator: "" }, this.updateDisplayData);
        break;
      case 3:
        trimLength = this.setTrimLength(buffer2, _operator);
        this.setState(
          { buffer2: buffer2.substring(0, buffer2.length - trimLength) },
          this.updateDisplayData
        );
        break;
      case 4:
        trimLength = this.setTrimLength(buffer2, _operator);
        this.setState(
          { buffer2: buffer2.substring(0, buffer2.length - trimLength) },
          this.updateDisplayData
        );
        break;

      default:
        break;
    }
  };

  setCondition = function (callback) {
    console.log("setCondtion hit", "callback", callback);
    const { buffer1, buffer2, operator, result, prevOperator } = this.state;

    console.log(
      "buffer1",
      buffer1,
      "buffer2",
      buffer2,
      "operator",
      operator,
      "result",
      result,
      "prevOperator",
      prevOperator
    );
    let condition;

    if (!buffer1 && !operator && !buffer2) {
      console.log("setting cond: 0");
      condition = 0;
    } else if (buffer1 && !operator && !buffer2 && !result) {
      console.log("setting cond: 1");
      condition = 1;
    } else if (buffer1 && operator && !buffer2 && !result) {
      console.log("setting cond: 2");
      condition = 2;
    } else if (buffer1 && operator && buffer2) {
      console.log("setting cond: 3");
      condition = 3;
    } else if (!buffer1 && operator && !buffer2 && !result && !prevOperator) {
      console.log("setting cond: 4");
      condition = 4;
    } else if (buffer1 && !operator && !buffer2 && result && prevOperator) {
      console.log("setting cond: 5");
      condition = 5;
    } else if (buffer1 && operator && buffer2 && result && prevOperator) {
      console.log("setting cond: 6");
      condition = 6;
    } else if (buffer1 && operator && !buffer2 && result && prevOperator) {
      console.log("setting cond: 7");
      condition = 7;
    } else {
      console.log("setCondition: condition not found");
      return;
    }
    this.setState({ condition: condition }, () => {
      if (callback) {
        callback.bind(this);
      }
    });
  };

  getUniChar = (operatorIn) => {
    const { keys } = this.state;
    return keys.filter((k) => k.value.toString() === operatorIn)[0].uniChar !==
      undefined
      ? keys.filter((k) => k.value.toString() === operatorIn)[0].uniChar
      : operatorIn;
  };

  packageDisplayData = (sectionName) => {
    let dataToGo = {};
    let section = { ...this.state[sectionName] };
    const sectionKeys = Object.keys(section);
    let data = {};
    let updatedKeys = [];

    data.buffer1 = this.state.buffer1;
    data.operator = this.state.operator;
    data.buffer2 = this.state.buffer2;
    sectionKeys.forEach((sk) => {
      Object.keys(data).forEach((dk) => {
        if (sk === dk && !updatedKeys.includes(dk)) {
          dataToGo[sk] = data[sk];
          updatedKeys.push(sk);
        } else {
          if (!updatedKeys.includes(sk)) {
            dataToGo[sk] = section[sk];
          }
        }
      });
    });
    return dataToGo;
  };

  setWidestCharWidth = (sectionLabel) => {
    console.log("setWidestCharWidth hit", sectionLabel);
    const section = { ...this.state[sectionLabel] };
    const currentlyStoredWidestCharWidth = section.widestCharWidth;
    let newComparisonCharWidth, newWidestCharWidth;
    const sectionWidth = this.displayRef.current.children[
      section.displayRefIndex
    ].offsetWidth;
    console.log(currentlyStoredWidestCharWidth, sectionWidth);

    if (sectionWidth) {
      if (currentlyStoredWidestCharWidth === 0) {
        console.log(
          "currentlyStoredWidestCharWidth === 0",
          currentlyStoredWidestCharWidth === 0
        );
        section.widestCharWidth = sectionWidth;
      } else {
        newComparisonCharWidth = sectionWidth - section.sectionWidth;
        if (newComparisonCharWidth > currentlyStoredWidestCharWidth) {
          section.widestCharWidth = newComparisonCharWidth;
          //
        }
      }
    }
    const dob = {};
    dob.label = sectionLabel;
    dob.parameter = "widestCharWidth";
    dob.value = section.widestCharWidth;
    dob.setCond = false;
    dob.callback = this.testCurrentSectionWidthAgainstMaxSectionWidth(
      dob.label
    );
    this.updateDisplayDataNew(this.state.displayDataObject);
  };

  componentDidMount = () => {};

  setSectionWidth = (sectionLabel) => {
    console.log("setSectionWidth hit", sectionLabel);
    let section = { ...this.state[sectionLabel] };
    const sectionWidth = this.displayRef.current.children[
      section.displayRefIndex
    ].offsetWidth;
    const dob = {};
    dob.label = sectionLabel;
    dob.parameter = "sectionWidth";
    dob.value = sectionWidth;
    dob.setCond = false;
    dob.callback = this.setWidestCharWidth(dob.label);
    // this.updateDisplayDataNew(dob);
  };

  testCurrentSectionWidthAgainstMaxSectionWidth = (sectionLabel) => {
    console.log(
      "testCurrentSectionWidthAgainstMaxSectionWidth hit",
      sectionLabel
    );
    let section = { ...this.state[sectionLabel] };

    let newComparisonCharWidth, newWidestCharWidth;
    let maxWidth = this.displayRef.current.offsetWidth;
    let sectionWidth = this.displayRef.current.children[section.displayRefIndex]
      .offsetWidth;
    let node = this.displayRef.current.children[section.displayRefIndex];
    let nodeStyle = window.getComputedStyle(node);
    let nodeHorizMargin =
      parseInt(nodeStyle.getPropertyValue("margin-right")) +
      parseInt(nodeStyle.getPropertyValue("margin-left"));

    console.log(
      "maxWidth - (section.sectionWidth - section.widestCharWidth - nodeHorizMargin)",
      maxWidth - (section.widestCharWidth + nodeHorizMargin) - sectionWidth,
      maxWidth - (section.widestCharWidth + nodeHorizMargin) - sectionWidth >=
        section.widestCharWidth
    );
    this.setState({
      maxWidthReached: !(
        maxWidth - (section.widestCharWidth + nodeHorizMargin) - sectionWidth >=
        section.widestCharWidth
      ),
    });
  };

  setSectionClass = (sectionLabel, state) => {
    console.log("setSectionClass hit", sectionLabel, "state", state);
    let section = { ...this.state[sectionLabel] };
    const dob = {};
    dob.label = sectionLabel;
    dob.parameter = "class";
    dob.setCond = false;

    const dobjArray = [];

    const dob2 = {};
    dob2.label = sectionLabel;
    dob2.parameter = "state";
    dob2.setCond = false;
    if (state === "disabled") {
      dob.value = section.normalClass + " " + section.disabledClass;
      dob2.value = "disabled";
    } else {
      dob.value = section.normalClass;
      dob2.value = "enabled";
    }

    dobjArray.push(dob2);
    dobjArray.push(dob);
  };

  setTrimLength = function (_buffer, _operator) {
    console.log(
      "setTrimLength Math.sign(buffer)=",
      _buffer,
      "|result =",
      Math.sign(_buffer),
      "_buffer.length",
      _buffer.length,
      "_operator",
      _operator
    );
    var _numChars = null;

    if (_operator) {
      var _uniChar = this.getUniChar(_operator);

      var _chars = _uniChar.split("");
      if (_chars) {
        _numChars = _chars.length;
        console.log(
          "_uniChar",
          _uniChar,
          "chars",
          _chars,
          "_numChars",
          _numChars
        );
        return _numChars;
      } else return _operator;
    } else {
      if (Math.sign(_buffer) === -1 && _buffer.length === 2) {
        return 2;
      } else {
        return 1;
      }
    }
  };

  processResultOutput = (result) => {
    const expo = (x, f) => {
      return Number.parseFloat(x).toExponential(f);
    };

    if (!Number.isInteger(result)) {
      let integerLength = result.countNumberParts(0);
      let decimalLength = result.countNumberParts(1);
      let totalLength = decimalLength + integerLength;
      const maxTotalLength = 9;

      // fix display of decimals ie 9.81 rather than 9.810000000000
      if (decimalLength > integerLength && totalLength > maxTotalLength) {
        if (decimalLength > maxTotalLength) {
          decimalLength = maxTotalLength;
          if (integerLength > 2) {
            decimalLength = decimalLength - integerLength;
          }
        }
      }
      // if ininity then catch it
      // also -Infinity
      if (
        decimalLength === Infinity ||
        decimalLength === Number.NEGATIVE_INFINITY
      ) {
        this.setState({ result: "err" });
        return;
      } else {
        //fix number of trailing zeroes
        result = result.toFixed(decimalLength);
      }
    }

    // switch to scientific notation if result exceeds display length
    if (result > 99999999999) {
      result = expo(result, 7);
    }
    result = "" + result;
    return result;
  };

  doTheMath = () => {
    console.log("doTheMath hit", this.state.readyForMaths);

    if (
      this.state.buffer1 + this.state.operator + this.state.buffer2 ===
      "404+545"
    ) {
      this.setState({ result: "Hello Baba, \uD83D\uDC95 you!" });
      return;
    } else if (
      this.state.buffer1 + this.state.operator + this.state.buffer2 ===
      "404-545"
    ) {
      this.setState({ result: "Bye Baba, miss you \uD83D\uDC96" });
      return;
    }

    // if (this.state.readyForMaths) {
    const { operator } = this.state;

    let num1 = +this.state.buffer1;
    let num2 = +this.state.buffer2;

    let { result } = this.state;

    const calculateResult = (op) => {
      switch (op) {
        case "+": {
          return num1 + num2;
        }
        case "-": {
          return num1 - num2;
        }
        case "x": {
          return num1 * num2;
        }
        case "y": {
          return Math.pow(num1, num2);
        }
        case "r": {
          return Math.sqrt(num1);
        }
        case "/": {
          return num1 / num2;
        }
        case "^": {
          if (num1 === 0) return 1;
          return num1 * num1;
        }
        default: {
          return null;
        }
      }
    };

    Number.prototype.countNumberParts = function (index) {
      if (Math.trunc(this.valueOf()) === this.valueOf()) return this.valueOf();
      return this.toString().split(".")[index].length || 0;
    };

    Number.prototype.splitDecimalNumberParts = function (index) {
      if (Math.floor(this.valueOf()) === this.valueOf()) return 0;
      return this.toString().split(".")[index].length || 0;
    };

    result = calculateResult(operator);

    // restrict number of decimals so not to exceed display length
    if (result) {
      result = this.processResultOutput(result);
    }
    // handle results that are not numbers ie. undefined, null, infinity
    else if (isNaN(result) || !isNaN(result / result) || !result === Infinity) {
      // result = "err";
      this.setState({ result: "err" });
      return;
    } else if (result === 0) {
      result = "" + result;
    }
    this.setState({ result }, this.postResultClearUp);
  };

  isMathsOp = (operator) => {
    console.log("isMathsOp hit : ", operator);
    return this.state.mathsOps.some((mo) => mo === operator);
  };

  isSingleBufferMathsOps = (operator) => {
    console.log("isSingleBufferMathsOps hit : ", operator);
    return this.state.singleBufferMathsOps.some((mo) => mo === operator);
  };

  isDoubleBufferMathsOps = (operator) => {
    console.log("isDoubleBufferMathsOps hit : ", operator);
    return this.state.doubleBufferMathsOps.some((mo) => mo === operator);
  };

  // utility functions
  postResultBufferProcessing = () => {
    console.log("postResultBufferProcessing hit", this.state);
    const { result } = this.state;
    if (
      this.state.nextOperator &&
      this.state.nextOperator !== "=" &&
      result !== "err"
    ) {
      this.setState({ buffer1: result, buffer2: "" });
    }
  };

  postResultClearUp = () => {
    this.postResultBufferProcessing();
    let { operator } = this.state;
    let { nextOperator } = this.state;

    // if user wants to continue calculating from previous result
    // they have pressed another maths operator
    // it is stored in nextOperator so we check this value and process it if present and not in err

    // "=" pressed - end calc routine
    if (this.state.result !== "err") {
      if (nextOperator && nextOperator === "=") {
        this.setState(
          {
            prevOperator: nextOperator,
            // operator: "",
            nextOperator: "",
            readyForMaths: false,
          },
          this.updateDisplayData
        );
        // "+,-,x,/" pressed - continue calc routine
      } else if (nextOperator && nextOperator !== "=") {
        this.setState(
          {
            prevOperator: operator,
            operator: nextOperator,
            nextOperator: "",
            readyForMaths: false,
          },
          this.updateDisplayData
        );
      }
    }
  };

  resetDisplayData = () => {
    let { calc, res } = { ...this.state };
    calc.widestCharWidth = 0;
    calc.sectionWidth = 0;
    res.widestCharWidth = 0;
    res.sectionWidth = 0;

    this.setState({ calc, res }, () => {
      this.setSectionClass("calc", "");
    });
  };

  clearAll = () => {
    this.setState({
      buffer1: "",
      buffer2: "",
      condition: 0,
      maxWidthReached: false,
      nextOperator: "",
      operator: "",
      prevOperator: "",
      readyForMaths: false,
      result: null,
    });
    this.resetDisplayData();
    console.clear();
  };

  setupDisplay = () => {
    return (
      <Display
        ref={this.displayRef}
        result={this.getDisplayResult()}
        calculation={this.getDisplayCalculation()}
        dispClass={this.state.disp.class}
        calcClass={this.state.calc.class}
        resClass={this.state.res.class}
      />
    );
  };

  setupPageHeader = () => {
    return (
      <React.Fragment>
        <div className="col">
          <div className="mt-2 mb-2 border-bottom">
            <h3>{this.state.headerTitle}</h3>
            <p>
              {this.state.author}{" "}
              <a
                className="float-right"
                href={this.state.headerLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                <i className={this.state.linkIcon}></i>{" "}
                <span title={this.state.linkTooltip}>
                  {this.state.linkText}
                </span>
              </a>
            </p>
          </div>
        </div>
      </React.Fragment>
    );
  };

  render = () => {
    console.log("hit ######### render ######### hit");
    return (
      <React.Fragment>
        <div className="container">
          <div className="row">{this.setupPageHeader()}</div>
          <div className="row">{this.setupDisplay()}</div>
          <div className="row">{this.setupKeyboards()}</div>
        </div>
      </React.Fragment>
    );
  };
}
export default App;
